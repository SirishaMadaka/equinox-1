FROM node:10.15.3-alpine

# Install bash
RUN apk add --update bash && rm -rf /var/cache/apk/*

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json yarn.lock /usr/src/app/
RUN yarn
COPY . ./ 
RUN yarn build

# Setting ENV Vars
ENV HOST 0.0.0.0
ENV PORT 8080

EXPOSE 8080:8080

CMD ["node", "./dist/server.js"]