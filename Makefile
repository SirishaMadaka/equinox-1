SHELL := /bin/bash
PATH := node_modules/.bin:$(PATH)

clean: 
	rm -rf dist/
	rm -rf node_modules/
	yarn