# Equinox PT

## Overview



## Requirements 
`• Node version 11.13.0 or later`

`• Npm version 6.5.0 or later`

`• Yarn version 1.16.0 or later`


## Coding standards

1. When installing new packages please use the yarn `--exact or -E` flag or mannually remove the `^` from the package addition. e.g. `yarn add react --exact` 

2. Folder structure - Component files live in the same folder as their styles and unit tests.

3. Filenames - kebabcase is used throughout the project. Components have the postfix `-component.jsx`. Tests use the `-test` postfix. Styles use no postifx.  e.g. `header-component.jsx`, `header-test.js`, & `header.scss` 

4. `console.log` has been set to warning in the linter intentionally. Please do not commit them unless for a very good reason. 

5. Static Type checking is done with [Flow](https://flow.org/en/) (Our React Native app uses Flow)

6. Style selectors are written in BEM (Block, Element, Modifier) methodology. For more information on BEM checkout: [https://css-tricks.com/bem-101/](https://css-tricks.com/bem-101/) 

7. Style font sizes are set with `font-size` set to  `62.5%` which translates to `1rem = 10pixles`. i.e. If you want a typeface to be roughly 15 pixels tall set it's `font-size: 1.5rem;`

## Installation: 

### 1. Project:

1. Install Node via [NVM](https://github.com/nvm-sh/nvm) 

2. Install yarn with the global flag: ```npm install -g yarn```

3. Clone this repository ```git clone https://bitbucket.org/vinesone/react-web-bolierplate/src/master/```

4. In the terminal / CMD: navigate to this project folder

5. Run: ```yarn install``` 

### 2. .ENV file
1. Duplicate: `.env.example`
2. Rename copy to `.env`
3. Fill in env values


### 3. Docker
*(For developement it isn't necessary to run the app in docker but it is good idea to test your code in the dockerized app as it mirrors what runs in other environments.) Also Note: The dockerized app runs on port `8080` not `8081` where the webpack dev server runs.*

1. Install [Docker](https://docs.docker.com/install/) 
2. In the terminal / CMD: navigate to this project folder
3. Run: ``` docker build . -t YOUR_DOCKER_USERNAME/react-web-boilerplate```
4. Run: ``` docker run -p 8080:8080 --name react-web-boilerplate  -d  YOUR_DOCKER_USERNAME/react-web-boilerplate:latest```
5. Navigate to [http://localhost:8080/](http://localhost:8080/)


## Development

For development this project uses webpack-dev-server for hot reloading. To run webpack: Run ```yarn run dev``` Also please note: If you update the hapi server file you will need to restart hapi. I plan on adding something like nodemon to automate the restart.

## Config

The config file is populated ENV variables injected at build time, and with the [dotenv](https://www.npmjs.com/package/dotenv) file. The dotenv file is run via arguments in the build scripts to eleminate the need for including it in the build bundle. It can be helpful for implementing feature flags in the future. 

The consts are exported from the file individually so you can access `PORT` & `HOST` for example by: 
`const { PORT, HOST } = require('../config');`

## Other Scripts / Commands
----

## Yarn

| Command      | Description |
| ----------- | ----------- |
| ```yarn run dev```   | Runs webpack dev server in dev mode for hot reloading  |
| ```yarn run dev:client```      | Transpiles and loads client side code into webpack-dev-server       |
| ```yarn run dev:server```   | Runs server in dev mode   |
| ```yarn run build```   | Builds production artifacts        |
| ```yarn run build:client```  | Builds client production artifacts       |
| ```yarn run build:server```   | Builds server production artifacts        |
| ```yarn run start```   | Starts Node server (Only works after build artifacts created )        |
| ```yarn run lint```   | Runs lint on the repo        |
| ```yarn run lint:style```   | Runs style lint on the repo        |
| ```yarn run lint:fn```   | Runs filename lint on the repo        |
| ```yarn run test```   | Runs unit tests (Jest)        |
| ```yarn run test:coverage```   | Runs unit tests (Jest) & generates coverage report       |
| ```yarn update```   | Interactively updates yarn dependancies        |
 

## Make
| Command      | Description |
| ----------- | ----------- |
| ```make clean```    | Removes node_module folder & re-installs project dependencies       |

## Commit Hooks

[Husky](https://github.com/typicode/husky) runs linting and tests on `pre-push`. *Runs: ( `yarn lint` && `yarn lint:style` && `yarn lint:fn` && `yarn test`)* If you need to push your branch quicky you can push with the `--no-verify` git flag. Never run this on a branch other than your own feature branch. 


