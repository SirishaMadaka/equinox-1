import React from 'react';
import '../App.css';
import { Link } from 'react-router-dom';

function SidebarNavigation() {
  return (
    <div>
      <header className="header">
        <div>
          <a href="\#" className="close-nav"><i className="icon-close" /></a>
          <h1 className="logo" id="logo">
            <a href="\#" data-bypass="true">Equinox PT</a>
          </h1>
          <div className="trainer-info">
            <a data-bypass="true" href="/account">
              <div className="user-avatar" id="user-avatar" />
              <p className="user-name" data-trainer-fname="">Jane</p>
            </a>
          </div>
        </div>
        <nav className="nav flex-column">
          <div>
            <Link to='/home' data-bypass="true" className="active nav-link">Home</Link>
            <Link to='/calender' data-bypass="true" className="active nav-link">Calendar</Link>
            <Link to='/client' data-bypass="true" className=" nav-link">Clients</Link>
            <Link to='/projections' data-bypass="true" className=" nav-link">Projections</Link>
            <Link to='/tierX' data-bypass="true" className=" nav-link">Tier X</Link>
          </div>
          <div className="bottom-details">
            <a href="https://equinoxprod.spm.ibmcloud.com/payeeweb/login.html?basic=true" target="_blank" rel="noopener noreferrer" className="">Commission Reports</a>
            <a href="https://equinox.learningecosystems.net/eco_player.php?id=1513" target="_blank" rel="noopener noreferrer" className="nav-link">Help</a>
            <a href="mailto:trainerapp.help@equinox.com?subject=Trainer App Feedback" data-rel="external" className="feedback"><span>trainerapp.help@equinox.com</span></a>
            <a href="\#" data-bypass="true" className="sign-out">Sign Out</a>
          </div>
        </nav>
      </header>
    </div>
  );
}

export default SidebarNavigation;
