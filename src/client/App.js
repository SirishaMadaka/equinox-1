import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import Home from './containers/Home';
import Calender from './containers/Calender';
import Projections from './containers/Projections';
import TierX from './containers/Tier-X';
import Client from './containers/Client';
import SidebarNavigation from './components/SidebarNavigation';
import { PrivateRoute } from './utility/PrivateRoute';

function App() {
  return (
    <div className="App">
      <Router>
        <SidebarNavigation />
        <PrivateRoute exact path='/' component={Home} />
        <PrivateRoute path='/home' component={Home} />
        <PrivateRoute path='/client' component={Client} />
        <PrivateRoute path='/calender' component={Calender} />
        <PrivateRoute path='/projections' component={Projections} />
        <PrivateRoute path='/tierX' component={TierX} />
      </Router>
    </div>
  );
}

export default App;
