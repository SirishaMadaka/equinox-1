import Immutable from 'immutable';
import {
  createStore,
  applyMiddleware,
  combineReducers
} from 'redux';
import { reducer as form } from 'redux-form/immutable';
import thunk from 'redux-thunk';
import {
  clients,
  creatingClient,
  creatingClientError,
  editingClient,
  editingClientError
} from './reducers/client';
import { RESET } from './constants/ClientActionTypes';

const initialState = {
  form: {},
  clients: {},
  creatingClient: {},
  creatingClientError: {},
  editingClient: {},
  editingClientError: {}
};

const resetState = Immutable.fromJS({
  clients: {}
});

const appReducer = combineReducers({
  form,
  clients,
  creatingClient,
  creatingClientError,
  editingClient,
  editingClientError
});

const rootReducer = (state, action) => {
  if (action.type === RESET) {
    return state.merge(resetState);
  }
  return appReducer(state, action);
};

const store = createStore(rootReducer, initialState, applyMiddleware(thunk));

export default store;
