import base64 from 'base-64';
import {
  StageURL, baseUrlStag, baseUrlQa, baseUrlProd, dataTokenBMS
} from './globalConstant';
import { callApi } from './apiService';

const getQueryStringParameters = () => {
  const dataToken = window.location.search.replace('?dataToken=', '') ? window.location.search.replace('?dataToken=', '') : dataTokenBMS;
  if (dataToken) {
    const decodeToken = base64.decode(dataToken.split('&')[0]);
    return (/^[?#]/.test(decodeToken) ? decodeToken.slice(1) : decodeToken)
      .split('&')
      .reduce((params, param) => {
        const [key, value] = param.split('=');
        const queryParams = params;
        queryParams[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
        return queryParams;
      }, {});
  }
  return null;
};

const getEnvApiUrl = () => {
  let apiBaseUrl = baseUrlStag;
  if (process.env.NODE_ENV === 'production') {
    apiBaseUrl = baseUrlProd;
  } else if (process.env.NODE_ENV === 'test') {
    apiBaseUrl = baseUrlQa;
  }
  return apiBaseUrl;
};

const frameUrl = () => {
  let framedUrl;
  const apiBaseUrl = getEnvApiUrl();
  const params = getQueryStringParameters();
  if (params) {
    if (params.mSelectCategory === 'BMS') {
      framedUrl = `${apiBaseUrl}/${params.mMosoId}/${params.equifitId}/${params.bmsDocumentId}`;
    } else {
      framedUrl = `${apiBaseUrl}/${params.mMosoId}/${params.equifitId}/${params.fmsDocumentId}`;
    }
  } else {
    framedUrl = StageURL;
  }
  return framedUrl;
};

const getData = () => {
  const URL = frameUrl();
  return new Promise((resolve, reject) => {
    callApi(`${URL}`, 'GET').then((response) => {
      resolve(response.data);
    }).catch((error) => {
      reject(error);
    });
  });
};

export { getData, getQueryStringParameters };
