const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const H2o2 = require('@hapi/h2o2');

const {
  PORT,
  HOST
} = require('../../config/config');

const provision = async () => {
  try {
    const server = Hapi.server({
      host: HOST,
      port: PORT,
      app: {},
      debug: { log: '*' },
      routes: {
        cors: {
          origin: ['*']
        }
      }
    });

    await server.register(Inert);
    await server.register(H2o2);

    server.route({
      method: 'GET',
      path: '/{path*}',
      handler: {
        directory: {
          path: './dist/public/',
          index: 'index.html',
          listing: false
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/img/{path*2}',
      handler: {
        directory: {
          path: './dist/public/img/',
          redirectToSlash: true,
          index: true,
          listing: true
        }
      }
    });

    // Server Status
    server.route({
      method: 'GET',
      path: '/status',
      handler: () => 'OK'
    });

    // Server Logs'
    server.events.on('log', (event, tags) => {
      if (tags.error) {
        // eslint-disable-next-line no-console
        console.log(`❌ Server error: ${event.error ? event.error.message : 'unknown'}`);
      }
    });

    await server.start();
    // eslint-disable-next-line no-console
    console.log(`✅  Hapi: server running at ${server.info.uri}`);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error('⚠️  Hapi: error starting server', err);
    process.exit(1);
  }
};

process.on('unhandledRejection', () => {
  // eslint-disable-next-line no-console
  process.exit(1);
});

process.once('SIGUSR2', () => ({
  gracefulShutdown: () => (process.kill(process.pid, 'SIGUSR2'))
}));

provision();
