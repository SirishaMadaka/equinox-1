module.exports = {
  "extends": "airbnb",
  "parser": "babel-eslint",
  "env": {
      "browser": true,
      "commonjs": true,
      "es6": true,
      "mocha": true,
      "node": true
  },
  "parserOptions": {
      "ecmaFeatures": {
          "jsx": false
      },
      "sourceType": "module"
  },
  "plugins": [
      "react"
  ],
  "rules": {
    "indent": [1,2],
    "comma-dangle": [2, "never"],
    "linebreak-style":"off",
    "jsx-quotes":"off",
    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
    "react/jsx-one-expression-per-line": "off",
    "import/prefer-default-export": "off",
  },
  "settings": {
    "import/resolver": {
      "webpack": {

      }
    }
  }
}