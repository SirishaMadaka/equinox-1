const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    main: path.resolve(__dirname, '../src/client/')
  },

  output: {
    path: path.resolve(__dirname, '../dist/public/'),
    filename: '[name].[hash].js',
    publicPath: '/',

  },
  devServer: {
    historyApiFallback: true,
  },

  module: {
    rules: [
      {
        test: /\.(js)$/,
        use: ['babel-loader', 'eslint-loader', 'source-map-loader'],
        exclude: /\.ejs|node_modules$/,
        enforce: 'pre'
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf|svg|png|jpg|gif)$/,
        use: ['file-loader']
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader?sourceMap', { loader: "postcss-loader?sourceMap",
        options: {
          plugins: () => [require('autoprefixer')]
        }}]
      }
    ]
  },

  plugins: [
    new CleanWebpackPlugin(),
    new CopyPlugin([{
      from: './src/client',
      to: './',
      ignore: ['.DS_Store']
    }]),
    new HtmlWebpackPlugin({
      title: 'Equinox PT | Home',
      template: './src/static/index.html',
      filename: 'index.html'
    }),
    require('autoprefixer')
  ],

  resolve: {
    alias: {

    },
    extensions: ['*', '.js', '.jsx']
  }
};
