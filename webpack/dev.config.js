const webpack = require('webpack');
const merge = require('webpack-merge');
const baseConfig = require('./base.config.js');

module.exports = merge(baseConfig, {
  module: {
    rules: [
      {
        test: /\.json$/,
        exclude: /\.ejs|node_modules$/,
        use: 'json-loader'
      }
    ]
  },

  devtool: 'cheap-module-eval-source-map',

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV) || JSON.stringify('development')
    }),
  ],

  node: {
    fs: 'empty'
  }
});
