// 'dotenv' is running as a dev dependency
// and is invoked with the -r flag
// when a npm script is invoked. ↓↓↓↓
const env = require('dotenv').config();
const path = require('path');

if (env.error) {
  console.log('Note: No .env file loaded');
}

module.exports = {
  PORT: process.env.PORT,
  HOST: process.env.HOST || 'localhost',
  NODE_ENV: process.env.PORT ? parseInt(process.env.PORT, 10) : 8080,
  IS_DEVELOPMENT: process.env.NODE_ENV === "development",
  ADAL: {
    APP_ID: '',
    CLIENT_SECRET: '',
    OBJECT_ID: '',
    AUTHORITY: '',
    TENANT: '',
    ENDPOINTS: {
      api: ''
    },
    CACHE_LOCATION: ''
  }
};
