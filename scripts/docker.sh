# !/bin/sh  
# Docker tagging to help in production envs 
# docker tag helloworld:latest yourorg/helloworld:$SHA1  
# docker tag helloworld:latest yourorg/helloworld:$BRANCH_NAME  
# docker tag helloworld:latest yourorg/build_$BUILD_NUM  

# Find IP of docker image
# docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' image id or name

# Remove Docker mounted images 
# docker rm -f $(docker ps -aq)

# Remove all images 
# docker system prune -a